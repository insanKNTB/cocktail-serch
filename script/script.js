
const btn = document.querySelector('button');

const inputSerch = document.querySelector('.input__serch');

const menuCoctail = document.querySelector('.information');

let answerSerch = [];

// close more infarmation
const wrapperMoreInfromation = document.querySelector('.more__information');

const listIngredients = document.querySelector('.structure__description');

// Serch type

const optionsType = document.querySelector('#serch__type');

// structure__description
const closeBtn = document.querySelector('.close');

function newItemCocktail (objCocktail) {
    
    return `
        <div class="cocktail__item">
            <img src="${objCocktail.strDrinkThumb}" alt="" srcset="">

            <p>${objCocktail.strDrink}</p>
        </div>
    `;

}

function addSerchCocktail() {

        for(let key of answerSerch[0]){
            menuCoctail.innerHTML += newItemCocktail(key);
        }

        allCoctailMore();

}

function allCoctailMore () {
    
    const allCoctail = document.querySelectorAll('.cocktail__item');    

    allCoctail.forEach((element, index) => {
        element.addEventListener('click', (e) => {

            if(e.path[0].classList.contains('cocktail__item') || e.path[1].classList.contains('cocktail__item')){
                
                wrapperMoreInfromation.style.display = 'flex';

                wrapperMoreInfromation.style.marginTop = window.pageYOffset + 'px';

                document.querySelector('body').style = 'overflow: hidden;';

                ingredientsView(answerSerch[0][index]);
            }

        });
    });

}

function ingredientsView (ingredientsObj) {

    listIngredients.innerHTML = '';

    for (let i = 1; i<= 15; i++){

        if (ingredientsObj['strIngredient' + i] === null){
            break;
        }

        listIngredients.innerHTML += `<li><span>${ingredientsObj['strIngredient' + i]}</span> : ${ingredientsObj['strMeasure' + i]}</li>`;

    }
    
    wrapperMoreInfromation.querySelector('.instructions__description').innerText = ingredientsObj.strInstructions;

}

function reguestData() {

    fetch(`https://www.thecocktaildb.com/api/json/v1/1/${optionsType.value + inputSerch.value.trim()}`)
        .then((data) => {

            answerSerch = [];
            return data.json();

        })
        .then((data) => {

            if (data.drinks === null) {

                alert('Pleace try again');
                inputSerch.value = '';

            } else {

                menuCoctail.innerHTML = '';
                answerSerch.push(data.drinks);
                addSerchCocktail();
                inputSerch.value = '';
                
            }
        });

}

reguestData();

btn.addEventListener('click', (event) => {
    event.preventDefault();

    if (optionsType.value === 'random.php'){
    
        inputSerch.value = '';
        reguestData();

    } else if (optionsType.value === 'search.php?f='){
    
        if (inputSerch.value.trim().length > 1 || inputSerch.value.trim().length < 0){
            alert('Pleace input only 1 letter');
        }else {
            inputSerch.value.trim() > '' ? reguestData() : alert('Please input something');
        }

    
    } else if (optionsType.value === 'lookup.php?i='){

        if (+inputSerch.value > 10999 && +inputSerch.value < 12000){
            inputSerch.value.trim() > '' ? reguestData() : alert('Please input something');
        }else{
            alert('Please input id cocktail (more than 10999 or less than 12000)');
        }
        
    }
    else if (optionsType.value === 'search.php?s='){
    
        reguestData();
    
    }

});

wrapperMoreInfromation.addEventListener('click', (event) => {
    
    if ((event.target.classList.contains('more__information')) || event.target.classList.contains('close')){
        
        wrapperMoreInfromation.style.display = 'none';
        document.querySelector('body').style = 'overflow-y: overlay;';
    
    }

});
